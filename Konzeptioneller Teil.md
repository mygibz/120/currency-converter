# Konzeptioneller Teil

von Yanik Ammann

Beim Start des Programmes bitte darauf achten, *View* zu starten.

#### Selbstbeschreibungsfähigkeit

##### Gelungene Aspekte

Es ist beim ersten Blick klar, wie die App funktioniert. Man hat zwei Textfelder und Dropdowns (je mit Titel). Beide Textfelder haben immer Zahlen drinn (wird also hier klar gemacht: Das ist eine Zahl), und beide Dropdowns zeigen immer eine Währung.

Wen man z.B. einen Buchstaben eingibt, dann wird das Textfeld rot umkreist. Dies teilt dem Nutzer mit, dass hier etwas nicht stimmt.

##### Verbesserungsmöglichkeiten

Keine Tooltips oder ähnliches. Ich denke persöhnlich nicht, dass dies hier notwendig ist, aber es währe trotzdem noch eine guten Möglichkeit dieses Programm zu verbessern.



#### Aufgabenangemessenheit

##### Gelungene Aspekte

Der Zweck dieser App ist es, dem Nutzer zu ermöglichen schlicht und einfach eine Währung in eine andere umzurechnen. Diese App tut genau das so simpel wie möglich und nichts weiter (also keine unnötigen Features, welche nicht erwartet werden).

##### Verbesserungsmöglichkeiten

Die Applikation ist eine Desktop applikation, muss dies aber nicht wirklich sein. Es würde auch reichen, eine Web-Applikation zu erstellen (zumindest sehe ich keine Vorteile einer Desktop App in diesem Anwendungsfall). Somit müsste der Nutzer nicht für etwas so simples eine ganze App herunterladen und könnte einfach seinen Browser verwenden.