﻿using System;
using System.Collections.Generic;

using Model;

namespace ViewModel
{
    /// <summary>
    /// ViewModel, that connects View and Model
    /// This is where most of the logic happens
    /// </summary>
    public class Controller : ModelBase
    {
        public Currency SourceCurrency {
            get => _sourceCurrency;
            set { _sourceCurrency = value; SourceToTarget(); }
        }
        private Currency _sourceCurrency;
        public Currency TargetCurrency {
            get => _targetCurrency;
            set { _targetCurrency = value; SourceToTarget(); }
        }
        private Currency _targetCurrency;

        public double TargetValue {
            get => _targetValue;
            set { _targetValue = value; TargetToSource(); }
        }
        private double _targetValue;
        public double SourceValue {
            get => _sourceValue;
            set { _sourceValue = value; SourceToTarget(); }
        }
        private double _sourceValue;

        public List<Currency> Currencies { get; } = ConversionData.Currencies;

        public Controller()
        {
            _sourceCurrency = Currencies.Find(currency => currency.Code == "CHF");
            _targetCurrency = Currencies.Find(currency => currency.Code == "EUR");
            SourceValue = 1;
        }

        void SourceToTarget()
        {
            _targetValue = Math.Round(ConversionData.Convert(_sourceValue, SourceCurrency, TargetCurrency), 2);
            OnPropertyChanged("TargetValue");
        }

        void TargetToSource()
        {
            _sourceValue = Math.Round(ConversionData.Convert(_targetValue, TargetCurrency, SourceCurrency), 2);
            OnPropertyChanged("SourceValue");
        }


    }

}
