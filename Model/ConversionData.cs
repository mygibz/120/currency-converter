﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{

    public class ConversionData
    {
        private static List<Currency> _currencies = new List<Currency> {
            new Currency("USD", "US Dollar"),
            new Currency("EUR", "Euro"),
            new Currency("JPY", "Yen"),
            new Currency("GPB", "Britisches Pfund"),
            new Currency("CHF", "Schweizer Franken"),
            new Currency("CAD", "Kanadische Dollar"),
            new Currency("AUD", "Australische Dollar"),
            new Currency("HKD", "Hong Kong Dollar"),
        };

        public static List<Currency> Currencies { get; } = _currencies;

        private static string[] _dimension = _currencies.Select(currency => currency.Code).ToArray();

        private static double[,] _rates = new double[8, 8]{
            {   1.0000,   1.0891, 0.0093,   1.2181,   1.0286,  0.7148,  0.6537,  0.1289 },
            {   0.9182,   1.0000, 0.0085,   1.1185,   0.9444,  0.6564,  0.6002,  0.1184 },
            { 107.6800, 117.2743, 1.0000, 131.1758, 110.7591, 76.9803, 70.3904, 13.8833 },
            {   0.8209,   0.8940, 0.0076,   1.0000,   0.8444,  0.5868,  0.5366,  0.1058 },
            {   0.9722,   1.0588, 0.0090,   1.1843,   1.0000,  0.6950,  0.6355,  0.1253 },
            {   1.3988,   1.5234, 0.0130,   1.7040,   1.4388,  1.0000,  0.9144,  0.1803 },
            {   1.5298,   1.6661, 0.0142,   1.8635,   1.5735,  1.0936,  1.0000,  0.1972 },
            {   7.7561,   8.4472, 0.0720,   9.4485,   7.9779,  5.5448,  5.0702,  1.0000 },
        };

        public static double Convert(double amount, Currency from, Currency to)
        {
            int row = Array.IndexOf(_dimension, from.Code);
            int col = Array.IndexOf(_dimension, to.Code);
            double rate = _rates[col, row];
            return amount * rate;
        }

    }

}
